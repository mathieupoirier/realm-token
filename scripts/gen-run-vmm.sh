#!/bin/bash
# Generate reference values, a DTB and a scripts containing the VMM command

use_virtconsole=true
use_edk2=false
use_direct_kernel=true
use_initrd=false
use_rme=true
verbose=false
vmm=qemu

KERNEL=$HOME/build/linux-cca/arch/arm64/boot/Image
INITRD=$HOME/build/buildroot-arm64-guest/images/rootfs.cpio
EDK2_DIR=$HOME/src/edk2-cca

OUTPUT_SCRIPT_DIR=$HOME/userspace/shr
OUTPUT_DTB_DIR=$HOME/userspace/shr/aarch64

RVSTORE_DIR=$HOME/src/rust-ccatoken/testdata

TEMP=$(getopt -o 'hv' --long 'help,edk2,disk-boot,initrd,serial,no-rme,kvmtool,verbose' -n 'gen-run-vmm' -- "$@")
if [ $? -ne 0 ]; then
    exit 1
fi

eval set -- "$TEMP"
unset TEMP
while true; do
    case "$1" in
    '--edk2')
        use_edk2=true
        ;;
    '--disk-boot')
        use_direct_kernel=false
        ;;
    '--initrd')
        use_initrd=true
        ;;
    '--serial')
        use_virtconsole=false
        ;;
    '--no-rme')
        use_rme=false
        ;;
    '--kvmtool')
        vmm=kvmtool
        ;;
    '-v'|'--verbose')
        verbose=true
        ;;
    '-h'|'--help')
        echo "Usage: $0"
        echo "  --disk-boot     boot from disk instead of direct kernel boot"
        echo "  --edk2          used ekd2 firmware"
        echo "  --initrd        use initrd as userspace instead of disk"
        echo "  --kvmtool       use kvmtool as VMM (default QEMU)"
        echo "  --no-rme        disable RME"
        echo "  --serial        use serial instead of virtconsole"
        echo "  -v  --verbose   be more verbose"
        exit 1
        ;;
    '--')
        shift
        break
        ;;
    *)
        echo 'Internal error!' >&2
        exit 1
        ;;
    esac
    shift
done

if [ $# -ne 0 ]; then
    echo "Unexpected argument '$1'" >&2
    exit 1
fi

if $use_edk2; then
    use_virtconsole=false
fi

declare -a CMD
declare -a KPARAMS

if $use_virtconsole; then
    KPARAMS+=(console=hvc0)
else
    KPARAMS+=(earlycon console=ttyAMA0)
fi

if ! $use_initrd; then
    KPARAMS+=(root=/dev/vda)
fi

if [ "$vmm" = "kvmtool" ]; then
    OUTPUT_SCRIPT=$OUTPUT_SCRIPT_DIR/run_kvm.sh
    OUTPUT_DTB=$OUTPUT_DTB_DIR/kvmtool-gen.dtb
    INPUT_RVSTORE=$RVSTORE_DIR/rv.json
    OUTPUT_RVSTORE=$RVSTORE_DIR/kvmtool-rv.json
    EDK2=$EDK2_DIR/Build/ArmVirtKvmtool-AARCH64/DEBUG_GCC5/FV/KVMTOOL_EFI.fd

    if $use_virtconsole; then
        CMD+=(--console virtio)
    else
        CMD+=(--console serial)
    fi

    if $use_rme; then
        CMD+=(--realm --restricted_mem --sve-vl=512)
    fi

    CMD+=(
        -c 2 -m 512
        -k guest_kernel
        --virtio-transport pci
        --irqchip=gicv3-its
        --pmu
        --network mode=user
        --9p /mnt/shr0,shr0
        --dtb aarch64/kvmtool-gen.dtb
        --debug
    )
    if $use_initrd; then
        CMD+=(-i guest_initrd)
    else
        CMD+=(-d guest_rootfs)
    fi

    APPEND=(-p "${KPARAMS[*]}")
else
    OUTPUT_SCRIPT=$OUTPUT_SCRIPT_DIR/run_qemu.sh
    OUTPUT_DTB=$OUTPUT_DTB_DIR/qemu-gen.dtb
    INPUT_RVSTORE=$RVSTORE_DIR/rv.json
    OUTPUT_RVSTORE=$RVSTORE_DIR/qemu-rv.json
    EDK2=$EDK2_DIR/Build/ArmVirtQemu-AARCH64/DEBUG_GCC5/FV/QEMU_EFI.fd

    if $use_rme; then
        CMD+=(-M confidential-guest-support=rme0 -object rme-guest,id=rme0,measurement-algo=sha512,personalization-value=abcd)
    fi

    if $use_virtconsole; then
        CMD+=(-nodefaults)
        CMD+=(-chardev stdio,mux=on,id=virtiocon0,signal=off -device virtio-serial-pci -device virtconsole,chardev=virtiocon0 -mon chardev=virtiocon0,mode=readline)
    fi

    if $use_direct_kernel; then
        CMD+=(-kernel guest_kernel)
        if $use_initrd; then
            CMD+=(-initrd guest_initrd)
        else
            CMD+=(-device virtio-blk-pci,drive=rootfs0)
            CMD+=(-drive format=raw,if=none,file=/tmp/guest_rootfs,id=rootfs0)
        fi
    else
        use_initrd=false
        CMD+=(-device virtio-blk-pci,drive=rootfs0)
        CMD+=(-drive format=raw,if=none,file=guest_disk,id=rootfs0)
    fi


    if $use_edk2; then
        CMD+=(-bios QEMU_EFI.fd)
    fi

    CMD+=(
        -cpu host -M virt -enable-kvm -M gic-version=3,its=on
        -smp 2 -m 512M
        -nographic
        -device virtio-net-pci,netdev=net0 -netdev user,id=net0
        -device virtio-net-pci,netdev=net1 -netdev user,id=net1
        -device virtio-net-pci,netdev=net2 -netdev user,id=net2
        -device virtio-net-pci,netdev=net3 -netdev user,id=net3
        -device virtio-9p-pci,fsdev=shr0,mount_tag=shr0
        -fsdev local,security_model=none,path=/mnt/shr0,id=shr0
        -dtb aarch64/qemu-gen.dtb
    )

    APPEND=(-append "${KPARAMS[*]}")
fi

pushd $HOME/src/realm-token/ >/dev/null
cargo run --features=ccatoken -- -c configs/qemu-max-8.2.conf -c configs/rmm-1.0-eac5.conf \
    --output-dtb $OUTPUT_DTB \
    -k "$KERNEL" \
    -i "$INITRD" \
    -f "$EDK2" \
    $($verbose && printf -- -v) \
    --input-rvstore $INPUT_RVSTORE --output-rvstore $OUTPUT_RVSTORE \
    $vmm "${CMD[@]}" "${APPEND[@]}"
popd >/dev/null

cat << EOF > $OUTPUT_SCRIPT
#!/bin/bash

# Rough platform detection
if [ -n "\$(dmesg | grep FVP)" ]; then
	GUEST_TTY=/dev/ttyAMA1
    # FVP 9p doesn't support whatever QEMU is trying to do the the rootfs. F_SETLK maybe?
    cp guest_rootfs /tmp/guest_rootfs
else
	GUEST_TTY=/dev/hvc1
    ln -fs /mnt/shr0/guest_rootfs /tmp/guest_rootfs
fi

set -x
EOF

if [ $vmm = kvmtool ]; then
    vmm_cmd="lkvm run"
else
    vmm_cmd=qemu-system-aarch64
fi

echo $vmm_cmd "${CMD[@]}" \
    $(printf "'%s' " "${APPEND[@]}")  \
    '>$GUEST_TTY <$GUEST_TTY &' \
    >> $OUTPUT_SCRIPT

