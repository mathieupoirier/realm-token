Given a VM configuration and the payload to run in the Realm, this tool
calculates the Realm Initial and Extended Measurements, needed for CCA
attestation.

This is a prototype, to experiment with Realm attestation and find out what
can/should be standardized.

Usage
=====

Build with:

    cargo build

or

    cargo build --features=ccatoken

The ccatoken feature adds a dependency on https://github.com/veraison/rust-ccatoken
It adds options --input-rvstore and --output-rvstore that produce a reference
value file to be consumed by ccatoken. More on that below.


Run with:

    target/debug/realm-token [options] <VMM> [vmm-args]

In the following example, the realm is started with kvmtool as VMM, using
direct kernel boot. The host is QEMU TCG.

    realm-token -c configs/qemu-max-8.2.conf -c configs/rmm-1.0-eac5.conf   (1)
        -k ~/build/linux-cca/arch/arm64/boot/Image                          (2)
        --output-dtb ~/vm/shr/qemu-gen.dtb                                  (3)
        kvmtool                                                             (4)
        -c 2 -m 256 --realm --console virtio --irqchip=gicv3-its
        --sve-vl=512 --pmu -k guest_kernel -d disk --9p shr

1. "-c" options provide host capabilities such as SVE vector length. Multiple
   config files can be provided, for example hardware, firmware, hypervisor and
   RMM capabilities. The capabilities can also be overriden with command-line
   arguments.
2. "-k", "-i", "-f" is the payload loaded into Realm memory
3. "--output-dtb" will contain the generated DTB, to be provided to the VMM
4. Arguments that follow the VMM name are those that will be passed to the VMM,
   and describe the VM configuration.

This displays RIM and REM, or with the ccatoken feature, produces a JSON file.
For example add:

    --input-rvstore ../ccatoken/testdata/rv.json --output-rvstore /tmp/rv.json

And then appraise a CCA token obtained from a running Realm:

    ccatoken appraise -e cca-token.cbor -r /tmp/rv.json

The CCA token can be obtained manually from within the running Realm. The
following writes it to a 9p shared directory:

    mount -t configfs none /sys/kernel/config
    report=/sys/kernel/config/tsm/report/report0
    mkdir $report
    dd if=/dev/zero bs=64 count=1 > $report/inblob  # challenge=0
    cp $report/outblob /mnt/shr0/cca-token.cbor
    rmdir $report


Realm token
===========

The Realm Token describes the state of the Realm VM at the time it is attested.
It comprises the Realm Initial Measurement (RIM), which includes initial
register and memory state, and Realm Extended Measurement (REM) computed by the
Realm itself, for example to measure a kernel image obtained from the host at
runtime.

An example:

* A client (the Relying Party) wants to ensure that a remote service provider
  is running in a Realm the correct payload, on the correct hardware and
  firmware.

* The client, or a trusted third party (the Verifier) establishes a secure
  connexion to the Realm, and issues a challenge, for example a random number
  that gets included into the realm token to guarantee freshness.

* The Realm asks RMM for an attestation token. RMM produces the Realm token.

* The platform signs the Realm token and the platform token, forming the CCA
  attestation token.

* The Realm sends the result of these calculations to the client.

* The client verifies the signatures and compares the returned Realm token with
  one it calculated itself. If everything matches, the client can share secrets
  with the Realm.

This tool helps with calculating a Realm token. The easiest way of computing
the Realm token would be to run the payload in the same environment, on a
machine that we own and trust. But neither client nor verifier might afford to
own such hardware. In addition, many changes to VM parameters such as number of
vCPUs, amount of memory, devices, would require running the whole payload again
in order to collect the corresponding token. We need to be able to compute the
token on demand, without running the payload.

